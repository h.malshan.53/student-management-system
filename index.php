<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Student Login</title>
    <link rel="icon" href="images/logo.png" type="" />
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>

<body class="bg-dark">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center" style="margin-top: 5%;">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card  border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="text-center my-3">Welcome to LMS</h1>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="div teacher"></div>
                                        </div>
                                        <div class="row">
                                            <div class="">
                                                <div class="text-center ">
                                                    <h1 class="h4 text-gray-900 mb-2 fw-bold">Welcome to Teacher login</h1>
                                                    <a href="loginT.php" class="btn btn-outline-primary ">Continue <i class="fa-solid fa-angles-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row mt-2">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="div student"></div>
                                        </div>
                                        <div class="row">
                                            <div class="">
                                                <div class="text-center ">
                                                    <h1 class="h4 text-gray-900 mb-2 fw-bold">Welcome to Student login</h1>
                                                    <a href="loginS.php" class="btn btn-outline-primary">Continue</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-lg-6">
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="div admin"></div>
                                        </div>
                                        <div class="row">
                                            <div class="">
                                                <div class="text-center ">
                                                    <h1 class="h4 text-gray-900 mb-2 fw-bold">Welcome to Admin login</h1>
                                                    <a href="loginA.php" class="btn btn-outline-primary">Continue</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="div officer"></div>
                                        </div>
                                        <div class="row">
                                            <div class="">
                                                <div class="text-center ">
                                                    <h1 class="h4 text-gray-900 mb-2 fw-bold">Welcome to Officer login</h1>
                                                    <a href="loginAO.php" class="btn btn-outline-primary">Continue</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <script src="login.js"></script>

</body>

</html>